class Vec{
    
    constructor( x = 0, y = 0 ){
	this.x = x;
	this.y = y;
    }

    add( vec ){	return new Vec( this.x + vec.x, this.y + vec.y ); }

    sub( vec ){	return new Vec( this.x - vec.x, this.y - vec.y ); }

    mult( A ){ return new Vec( this.x * A, this.y * A ); }

    div( A ){ return new Vec( this.x / A, this.y / A ); }
    
    len(){ return Math.sqrt( this.x*this.x + this.y*this.y ); }
    
    normalize(){
	let len = this.len();
	this.x = this.x / len;
	this.y = this.y / len;
	return this;
    }

    print(){ console.log( "x: " + this.x + ", y: " + this.y  ); }
}

class Mat{

    constructor( a00, a10, a01, a11 ){
	this.a00 = a00;
	this.a10 = a10;
	this.a01 = a01;
	this.a11 = a11;
    };

    det(){ return this.a00*this.a11 - this.a10*this.a01; };
};
    
class Shape{

    constructor(){ this.ps = {}; }
    
    side( p0, p1, p){ return (p1.y - p0.y)*(p.x - p0.x) + (-p1.x + p0.x)*(p.y - p0.y); };

    move( p ){
	var move_v = p.sub( this.ps[0] );
	for( var key in this.ps ){
	    this.ps[key] = this.ps[key].add( move_v );
	};
	this.init();
    };

    scale( m ){
	for( var key in this.ps ){
	    this.ps[key] = this.ps[0].add( this.ps[key].sub( this.ps[0] ).mult( m ) );
	};
	this.init();
    }
    
    print(){
	for( var key in this.ps ){
	    console.log( "Point " + key + ": x " + this.ps[key].x + ", y " + this.ps[key].y );
	};
    };
};

class Triangle extends Shape{
    
    constructor( p0, p1, p2 ){
	super();
	this.ps = { 0: p0,
		    1: p1,
		    2: p2 };
    }

    init(){};
 	
    pointInside( p ){
	var checkSide0 = this.side(this.ps[0], this.ps[1], p) >= 0;
	var checkSide1 = this.side(this.ps[1], this.ps[2], p) >= 0;
	var checkSide2 = this.side(this.ps[2], this.ps[0], p) >= 0;
	return checkSide0 && checkSide1 && checkSide2;
    };
};

class Star extends Shape{

    constructor( p0, p1, p2, p3, p4 ){
	super();
	this.ps = { 0: p0,
		    1: p1,
		    2: p2,
		    3: p3,
		    4: p4 };
	this.init();
    };

    init(){
	this.poly0 = new Triangle( this.ps[0], this.ps[2], get_2d_intersection( this.ps[0], this.ps[3], this.ps[2], this.ps[4] ) );
	this.poly1 = new Triangle( this.ps[1], get_2d_intersection( this.ps[1], this.ps[3], this.ps[4], this.ps[2] ), this.ps[4] );
	this.poly2 = new Triangle( this.ps[0], get_2d_intersection( this.ps[0], this.ps[2], this.ps[3], this.ps[1] ), this.ps[3] );
    };
    
    pointInside( p ){ return this.poly0.pointInside( p ) || this.poly1.pointInside( p ) || this.poly2.pointInside( p ); };
};

class Moon extends Shape{

    constructor( p0, p1 ){
	super();
	this.ps = { 0: p0,
		    1: p1 };
	this.init();
    };

    init(){
	this.outer = this.ps[0].add( this.ps[1].sub( this.ps[0] ).div( 2 ) );
	this.outer_rad = this.ps[1].sub( this.ps[0] ).len()/2;
	this.inner = this.ps[0].add( this.ps[1].sub( this.ps[0] ).div( 2 ).sub( get_perp_v( this.ps[0], this.ps[1] ).mult(this.ps[1].sub( this.ps[0] ).len()/4) ) );
	this.inner_rad = this.inner.sub( this.ps[1] ).len();
    };
    
    pointInside( p ){
	var outside_inner = p.sub( this.inner).len() > this.inner_rad;
	var inside_outer = p.sub( this.outer).len() < this.outer_rad;
	return outside_inner && inside_outer;
    };
};



//Utility functions



function get_perp_v( p0, p1 ){
    if( p0.x == p1.x ){
	return new Vec( 1, 0 );
    }
    return new Vec( -(p1.y - p0.y)/(p1.x - p0.x), 1 ).normalize();
}

function get_2d_intersection( p1, p2, p3, p4 ){

    aa1 = new Mat( p1.x, p1.y, p2.x, p2.y );
    ba1 = new Mat( p1.x,    1, p2.x,    1 );
    ab1 = new Mat( p3.x, p3.y, p4.x, p4.y );
    bb1 = new Mat( p3.x,    1, p4.x,    1 );

    aa2 = ba1;
    ba2 = new Mat( p1.y,    1, p2.y,    1 );
    ab2 = bb1;
    bb2 = new Mat( p3.y,    1, p4.y,    1 );

    aa3 = aa1;
    ba3 = ba2;
    ab3 = ab1;
    bb3 = bb2;

    aa4 = ba1;
    ba4 = ba2;
    ab4 = bb1;
    bb4 = bb2;
    
    Px = ( new Mat(aa1.det(), ba1.det(), ab1.det(), bb1.det() ) ).det() / ( new Mat(aa2.det(), ba2.det(), ab2.det(), bb2.det() ) ).det();
    Py = ( new Mat(aa3.det(), ba3.det(), ab3.det(), bb3.det() ) ).det() / ( new Mat(aa4.det(), ba4.det(), ab4.det(), bb4.det() ) ).det();
    
    return new Vec( Px, Py );
};
