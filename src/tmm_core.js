from __future__ import division, print_function, absolute_import

from numpy import cos, inf, zeros, array, exp, conj, nan, isnan, pi, sin, seterr
from numpy.lib.scimath import arcsin

import numpy as np

import sys
EPSILON = sys.float_info.epsilon // typical floating-point calculation error

function make_2x2_array(a, b, c, d, dtype=float){
    /*
    Makes a 2x2 numpy array of [[a,b],[c,d]]

    Same as "numpy.array([[a,b],[c,d]], dtype=float)", but ten times faster
    */
    my_array = np.empty((2,2), dtype=dtype)
    my_array[0,0] = a
    my_array[0,1] = b
    my_array[1,0] = c
    my_array[1,1] = d
    return my_array

function is_forward_angle(n, theta){
    /*
    if a wave is traveling at angle theta from normal in a medium with index n,
    calculate whether or not this is the forward-traveling wave (i.e., the one
    going from front to back of the stack, like the incoming or outgoing waves,
    but unlike the reflected wave). For real n & theta, the criterion is simply
    -pi/2 < theta < pi/2, but for complex n & theta, it's more complicated.
    See https://arxiv.org/abs/1603.02720 appendix D. If theta is the forward
    angle, then (pi-theta) is the backward angle and vice-versa.
    */
    assert n.real * n.imag >= 0, ("For materials with gain, it's ambiguous which "
                                  "beam is incoming vs outgoing. See "
                                  "https://arxiv.org/abs/1603.02720 Appendix C.\n"
                                  "n: " + str(n) + "   angle: " + str(theta))
    ncostheta = n * cos(theta);
    if (abs(ncostheta.imag) > 100 * EPSILON){
        // Either evanescent decay or lossy medium. Either way, the one that
        // decays is the forward-moving wave
        answer = (ncostheta.imag > 0);
    }else{
        // Forward is the one with positive Poynting vector
        // Poynting vector is Re[n cos(theta)] for s-polarization or
        // Re[n cos(theta*)] for p-polarization, but it turns out they're consistent
        // so I'll just assume s then check both below
        answer = (ncostheta.real > 0);
    }
    // convert from numpy boolean to the normal Python boolean
    answer = bool(answer);
    // double-check the answer ... can't be too careful!
    error_string = ("It's not clear which beam is incoming vs outgoing. Weird"
                    " index maybe?\n"
                    "n: " + str(n) + "   angle: " + str(theta))
    if (answer){
        assert ncostheta.imag > -100 * EPSILON, error_string;
        assert ncostheta.real > -100 * EPSILON, error_string;
        assert (n * cos(theta.conjugate())).real > -100 * EPSILON, error_string;
    }else{
        assert ncostheta.imag < 100 * EPSILON, error_string;
        assert ncostheta.real < 100 * EPSILON, error_string;
        assert (n * cos(theta.conjugate())).real < 100 * EPSILON, error_string;
    }
    return answer;
}

function snell(n_1, n_2, th_1){
    /*
    return angle theta in layer 2 with refractive index n_2, assuming
    it has angle th_1 in layer with refractive index n_1. Use Snell's law. Note
    that "angles" may be complex!!
    */
    // Important that the arcsin here is numpy.lib.scimath.arcsin, not
    // numpy.arcsin! (They give different results e.g. for arcsin(2).)
    th_2_guess = arcsin(n_1*np.sin(th_1) / n_2);
    if(is_forward_angle(n_2, th_2_guess) ){
        return th_2_guess;
    }else{
        return pi - th_2_guess;
    }
}

function list_snell(n_list, th_0){
    /*
    return list of angle theta in each layer based on angle th_0 in layer 0,
    using Snell's law. n_list is index of refraction of each layer. Note that
    "angles" may be complex!!
    */
    // Important that the arcsin here is numpy.lib.scimath.arcsin, not
    // numpy.arcsin! (They give different results e.g. for arcsin(2).)
    angles = arcsin(n_list[0]*np.sin(th_0) / n_list);
    // The first and last entry need to be the forward angle (the intermediate
    // layers don't matter, see https://arxiv.org/abs/1603.02720 Section 5)
    if (!(is_forward_angle(n_list[0], angles[0]))){
        angles[0] = pi - angles[0];
    };
    if (!(is_forward_angle(n_list[-1], angles[-1]))){
        angles[-1] = pi - angles[-1];
    };
    return angles;
}


function interface_r(polarization, n_i, n_f, th_i, th_f){
    /*
    reflection amplitude (from Fresnel equations)

    polarization is either "s" or "p" for polarization

    n_i, n_f are (complex) refractive index for incident and final

    th_i, th_f are (complex) propegation angle for incident and final
    (in radians, where 0=normal). "th" stands for "theta".
    */
    if(polarization == 's'){
        return ((n_i * cos(th_i) - n_f * cos(th_f)) /
                (n_i * cos(th_i) + n_f * cos(th_f)));
    }else if (polarization == 'p'){
        return ((n_f * cos(th_i) - n_i * cos(th_f)) /
                (n_f * cos(th_i) + n_i * cos(th_f)));
    }else{
	throw "Polarization must be 's' or 'p'";
    }
}

function interface_t(polarization, n_i, n_f, th_i, th_f){
    /*
    transmission amplitude (frem Fresnel equations)

    polarization is either "s" or "p" for polarization

    n_i, n_f are (complex) refractive index for incident and final

    th_i, th_f are (complex) propegation angle for incident and final
    (in radians, where 0=normal). "th" stands for "theta".
    */
    if (polarization == 's'){
        return 2 * n_i * cos(th_i) / (n_i * cos(th_i) + n_f * cos(th_f));
    } else if (polarization == 'p'){
        return 2 * n_i * cos(th_i) / (n_f * cos(th_i) + n_i * cos(th_f));
    }else{
	throw "Polarization must be 's' or 'p'";
    }
}

function R_from_r(r){
    /*
    Calculate reflected power R, starting with reflection amplitude r.
    */
    return abs(r)**2;
}

function T_from_t(pol, t, n_i, n_f, th_i, th_f){
    /*
    Calculate transmitted power T, starting with transmission amplitude t.

    n_i,n_f are refractive indices of incident and final medium.

    th_i, th_f are (complex) propegation angles through incident & final medium
    (in radians, where 0=normal). "th" stands for "theta".

    In the case that n_i,n_f,th_i,th_f are real, formulas simplify to
    T=|t|^2 * (n_f cos(th_f)) / (n_i cos(th_i)).

    See manual for discussion of formulas
    */
    if (pol == 's'){
        return abs(t**2) * (((n_f*cos(th_f)).real) / (n_i*cos(th_i)).real);
    } else if (pol == 'p'){
        return abs(t**2) * (((n_f*conj(cos(th_f))).real) /
                            (n_i*conj(cos(th_i))).real);
    } else {
	throw "Polarization must be 's' or 'p'";
    }
}

function interface_R(polarization, n_i, n_f, th_i, th_f){
    /*
    Fraction of light intensity reflected at an interface.
    */
    r = interface_r(polarization, n_i, n_f, th_i, th_f);
    return R_from_r(r);
}

function interface_T(polarization, n_i, n_f, th_i, th_f){
    /*
    Fraction of light intensity transmitted at an interface.
    */
    t = interface_t(polarization, n_i, n_f, th_i, th_f);
    return T_from_t(polarization, t, n_i, n_f, th_i, th_f);
}
    
function coh_tmm_reverse(pol, n_list, d_list, th_0, lam_vac){
    /*
    Reverses the order of the stack then runs coh_tmm.
    */
    th_f = snell(n_list[0], n_list[-1], th_0);
    return coh_tmm(pol, n_list[::-1], d_list[::-1], th_f, lam_vac);
}


function inc_group_layers(n_list, d_list, c_list){
    /*
    Helper function for inc_tmm. Groups and sorts layer information.

    See coh_tmm for definitions of n_list, d_list.

    c_list is "coherency list". Each entry should be 'i' for incoherent or 'c'
    for 'coherent'.

    A "stack" is a group of one or more consecutive coherent layers. A "stack
    index" labels the stacks 0,1,2,.... The "within-stack index" counts the
    coherent layers within the stack 1,2,3... [index 0 is the incoherent layer
    before the stack starts]

    An "incoherent layer index" labels the incoherent layers 0,1,2,...

    An "alllayer index" labels all layers (all elements of d_list) 0,1,2,...

    Returns info about how the layers relate:

    * stack_d_list[i] = list of thicknesses of each coherent layer in the i'th
      stack, plus starting and ending with "inf"
    * stack_n_list[i] = list of refractive index of each coherent layer in the
      i'th stack, plus the two surrounding incoherent layers
    * all_from_inc[i] = j means that the layer with incoherent index i has
      alllayer index j
    * inc_from_all[i] = j means that the layer with alllayer index i has
      incoherent index j. If j = nan then the layer is coherent.
    * all_from_stack[i1][i2] = j means that the layer with stack index i1 and
      within-stack index i2 has alllayer index j
    * stack_from_all[i] = [j1 j2] means that the layer with alllayer index i is
      part of stack j1 with withinstack-index j2. If stack_from_all[i] = nan
      then the layer is incoherent
    * inc_from_stack[i] = j means that the i'th stack comes after the layer
      with incoherent index j, and before the layer with incoherent index j+1.
    * stack_from_inc[i] = j means that the layer with incoherent index i comes
      immediately after the j'th stack. If j=nan, it is not immediately
      following a stack.
    * num_stacks = number of stacks
    * num_inc_layers = number of incoherent layers
    * num_layers = number of layers total
    */

    if ( (n_list.ndim != 1) or (d_list.ndim != 1) ){
        throw "Problem with n_list or d_list!";
    }
    if ( (d_list[0] != inf) or (d_list[-1] != inf) ){
        throw 'd_list must start and end with inf!';
    }
    if ( (c_list[0] != 'i') or (c_list[-1] != 'i') ){
        throw 'c_list should start and end with "i"';
    }
    if ( not n_list.size == d_list.size == len(c_list) ){
        throw 'List sizes do not match!';
    }
    inc_index = 0;
    stack_index = 0;
    stack_d_list = [];
    stack_n_list = [];
    all_from_inc = [];
    inc_from_all = [];
    all_from_stack = [];
    stack_from_all = [];
    inc_from_stack = [];
    stack_from_inc = [];
    stack_in_progress = false;
    for( alllayer_index = 0; alllayer_index = n_list.size; alllayer_index++ ){
        if( c_list[alllayer_index] == 'c'){ //coherent layer
            inc_from_all.append(nan);
            if( ! stack_in_progress ){ //this layer is starting new stack
                stack_in_progress = true;
                ongoing_stack_d_list = [inf, d_list[alllayer_index]];
                ongoing_stack_n_list = [n_list[alllayer_index-1],
                                        n_list[alllayer_index]];
                stack_from_all.append([stack_index,1]);
                all_from_stack.append([alllayer_index-1, alllayer_index]);
                inc_from_stack.append(inc_index-1);
                within_stack_index = 1;
            } else { //another coherent layer in the same stack
                ongoing_stack_d_list.append(d_list[alllayer_index]);
                ongoing_stack_n_list.append(n_list[alllayer_index]);
                within_stack_index += 1;
                stack_from_all.append([stack_index, within_stack_index]);
                all_from_stack[-1].append(alllayer_index);
	    }
        } else if( c_list[alllayer_index] == 'i'){ //incoherent layer
            stack_from_all.append(nan);
            inc_from_all.append(inc_index);
            all_from_inc.append(alllayer_index);
            if (! stack_in_progress){ //previous layer was also incoherent
                stack_from_inc.append(nan);
            } else { //previous layer was coherent
                stack_in_progress = false;
                stack_from_inc.append(stack_index);
                ongoing_stack_d_list.append(inf);
                stack_d_list.append(ongoing_stack_d_list);
                ongoing_stack_n_list.append(n_list[alllayer_index]);
                stack_n_list.append(ongoing_stack_n_list);
                all_from_stack[-1].append(alllayer_index);
                stack_index += 1;
	    }
	    inc_index += 1;
	}
    } else {
        throw "Error: c_list entries must be 'i' or 'c'!";
    }
    return {'stack_d_list':stack_d_list,
            'stack_n_list':stack_n_list,
            'all_from_inc':all_from_inc,
            'inc_from_all':inc_from_all,
            'all_from_stack':all_from_stack,
            'stack_from_all':stack_from_all,
            'inc_from_stack':inc_from_stack,
            'stack_from_inc':stack_from_inc,
            'num_stacks':len(all_from_stack),
            'num_inc_layers':len(all_from_inc),
            'num_layers':len(n_list)};

function inc_tmm(pol, n_list, d_list, c_list, th_0, lam_vac){
/*
    Incoherent, or partly-incoherent-partly-coherent, transfer matrix method.

    See coh_tmm for definitions of pol, n_list, d_list, th_0, lam_vac.

    c_list is "coherency list". Each entry should be 'i' for incoherent or 'c'
    for 'coherent'.

    If an incoherent layer has real refractive index (no absorption), then its
    thickness doesn't affect the calculation results.

    See https://arxiv.org/abs/1603.02720 for physics background and some
    of the definitions.

    Outputs the following as a dictionary:

    * R--reflected wave power (as fraction of incident)
    * T--transmitted wave power (as fraction of incident)
    * VW_list-- n'th element is [V_n,W_n], the forward- and backward-traveling
      intensities, respectively, at the beginning of the n'th incoherent medium.
    * coh_tmm_data_list--n'th element is coh_tmm_data[n], the output of
      the coh_tmm program for the n'th "stack" (group of one or more
      consecutive coherent layers).
    * coh_tmm_bdata_list--n'th element is coh_tmm_bdata[n], the output of the
      coh_tmm program for the n'th stack, but with the layers of the stack
      in reverse order.
    * stackFB_list--n'th element is [F,B], where F is light traveling forward
      towards the n'th stack and B is light traveling backwards towards the n'th
      stack.
    * num_layers-- total number both coherent and incoherent.
    * power_entering_list--n'th element is the normalized Poynting vector
      crossing the interface into the n'th incoherent layer from the previous
      (coherent or incoherent) layer.
    * Plus, all the outputs of inc_group_layers

    */
    

    // Input tests
    if( (np.real_if_close(n_list[0]*np.sin(th_0))).imag != 0){
        throw 'Error in n0 or th0!';
    };

    group_layers_data = inc_group_layers(n_list, d_list, c_list);
    num_inc_layers = group_layers_data['num_inc_layers'];
    num_stacks = group_layers_data['num_stacks'];
    stack_n_list = group_layers_data['stack_n_list'];
    stack_d_list = group_layers_data['stack_d_list'];
    all_from_stack = group_layers_data['all_from_stack'];
    all_from_inc = group_layers_data['all_from_inc'];
    all_from_stack = group_layers_data['all_from_stack'];
    stack_from_inc = group_layers_data['stack_from_inc'];
    inc_from_stack = group_layers_data['inc_from_stack'];

    // th_list is a list with, for each layer, the angle that the light travels
    // through the layer. Computed with Snell's law. Note that the "angles" may be
    // complex!
    th_list = list_snell(n_list, th_0);

    // coh_tmm_data_list[i] is the output of coh_tmm for the i'th stack
    coh_tmm_data_list = [];
    // coh_tmm_bdata_list[i] is the same stack as coh_tmm_data_list[i] but
    // with order of layers reversed
    coh_tmm_bdata_list = [];
    for( i = 0; i != num_stacks; i++ ){
        coh_tmm_data_list.append(coh_tmm(pol, stack_n_list[i],
                                         stack_d_list[i],
                                         th_list[all_from_stack[i][0]],
                                         lam_vac));
        coh_tmm_bdata_list.append(coh_tmm_reverse(pol, stack_n_list[i],
                                                  stack_d_list[i],
                                                  th_list[all_from_stack[i][0]],
                                                  lam_vac));
    }
    // P_list[i] is fraction not absorbed in a single pass through i'th incoherent
    // layer.
    P_list = zeros(num_inc_layers);
    for( inc_index = 1; inc_index != num_inc_layers-1; inc_index++ ){ //skip 0'th and last (infinite)
        i = all_from_inc[inc_index];
        P_list[inc_index] = exp(-4 * np.pi * d_list[i]
				* (n_list[i] * cos(th_list[i])).imag / lam_vac);
        // For a very opaque layer, reset P to avoid divide-by-0 and similar
        // errors.
        if( P_list[inc_index] < 1e-30 ){
            P_list[inc_index] = 1e-30;
	}
    }
    // T_list[i,j] and R_list[i,j] are transmission and reflection powers,
    // respectively, coming from the i'th incoherent layer, going to the j'th
    // incoherent layer. Only need to calculate this when j=i+1 or j=i-1.
    // (2D array is overkill but helps avoid confusion.)
    // initialize these arrays
    T_list = zeros((num_inc_layers, num_inc_layers));
    R_list = zeros((num_inc_layers, num_inc_layers));
    for( inc_index = 0; inc_index != num_inc_layers-1; inc_index++ ){ //looking at interface i -> i+1
        alllayer_index = all_from_inc[inc_index];
        nextstack_index = stack_from_inc[inc_index+1];
	R_list[inc_index, inc_index+1] = (
	       interface_R(pol, n_list[alllayer_index],
			   n_list[alllayer_index+1],
			   th_list[alllayer_index],
			   th_list[alllayer_index+1]));
	T_list[inc_index, inc_index+1] = (
	       interface_T(pol, n_list[alllayer_index],
			   n_list[alllayer_index+1],
			   th_list[alllayer_index],
			   th_list[alllayer_index+1]));
	R_list[inc_index+1, inc_index] = (
	       interface_R(pol, n_list[alllayer_index+1],
			   n_list[alllayer_index],
			   th_list[alllayer_index+1],
			   th_list[alllayer_index]));
	T_list[inc_index+1, inc_index] = (
	       interface_T(pol, n_list[alllayer_index+1],
			   n_list[alllayer_index],
			   th_list[alllayer_index+1],
			   th_list[alllayer_index]));
    }
    // L is the transfer matrix from the i'th to (i+1)st incoherent layer, see
    // manual
    L_list = [nan]; // L_0 is not defined because 0'th layer has no beginning.
    Ltilde = (array([[1,-R_list[1,0]],
                     [R_list[0,1],
                      T_list[1,0]*T_list[0,1] - R_list[1,0]*R_list[0,1]]])
              / T_list[0,1]);
    for( i=1; i != num_inc_layers-1; i++){
        L = np.dot(
           array([[1/P_list[i],0],[0,P_list[i]]]),
           array([[1,-R_list[i+1,i]],
                  [R_list[i,i+1],
                   T_list[i+1,i]*T_list[i,i+1] - R_list[i+1,i]*R_list[i,i+1]]])
        ) / T_list[i,i+1];
        L_list.append(L);
        Ltilde = np.dot(Ltilde,L);
    }
    T = 1 / Ltilde[0,0];
    R = Ltilde[1,0] / Ltilde[0,0];

    // VW_list[n] = [V_n, W_n], the forward- and backward-moving intensities
    // at the beginning of the n'th incoherent layer. VW_list[0] is undefined
    // because 0'th layer has no beginning.
    VW_list=zeros((num_inc_layers, 2));
    VW_list[0,:] = [nan, nan];
    VW = array([[T],[0]]);
    VW_list[-1,:] = np.transpose(VW);
    for( i=num_inc_layers-2 i!=0; i--){
        VW = np.dot(L_list[i], VW);
        VW_list[i,:] = np.transpose(VW);
    }
    // stackFB_list[n]=[F,B] means that F is light traveling forward towards n'th
    // stack and B is light traveling backwards towards n'th stack.
    // Reminder: inc_from_stack[i] = j means that the i'th stack comes after the
    // layer with incoherent index j.
    stackFB_list = [];
    for (const [stack_index, prev_inc_index] of inc_from_stack.entries()) {
        if( prev_inc_index == 0) { //stack starts right after semi-infinite layer.
            F = 1;
	} else {
            F = VW_list[prev_inc_index][0] * P_list[prev_inc_index];
	}
        B = VW_list[prev_inc_index+1][1];
        stackFB_list.append([F,B]);
    }
    // power_entering_list[i] is the normalized Poynting vector crossing the
    // interface into the i'th incoherent layer from the previous (coherent or
    // incoherent) layer. See manual.
    power_entering_list = [1]; //"1" by convention for infinite 0th layer.
    for( i=1; i!=num_inc_layers; i++){
        prev_stack_index = stack_from_inc[i];
        if( isnan(prev_stack_index) ){
            //case where this layer directly follows another incoherent layer
            if( i == 1){ //special case because VW_list[0] & A_list[0] are undefined
                power_entering_list.append(T_list[0,1]
                                           - VW_list[1][1]*T_list[1,0]);
            }else{
                power_entering_list.append(
                    VW_list[i-1][0]*P_list[i-1]*T_list[i-1,i]
			- VW_list[i][1]*T_list[i,i-1]);
            }
	}else{ //case where this layer follows a coherent stack
            power_entering_list.append(
                stackFB_list[prev_stack_index][0] *
                    coh_tmm_data_list[prev_stack_index]['T']
                    - stackFB_list[prev_stack_index][1] *
                    coh_tmm_bdata_list[prev_stack_index]['power_entering']);
	}
    }
    var ans = {'T':T, 'R':R, 'VW_list':VW_list,
               'coh_tmm_data_list':coh_tmm_data_list,
               'coh_tmm_bdata_list':coh_tmm_bdata_list,
               'stackFB_list':stackFB_list,
               'power_entering_list':power_entering_list};
    ans.update(group_layers_data);
    return ans;
}
