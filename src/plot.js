elements = {}

function setup(){
    createCanvas(200, 200);
    noLoop();
}

var data = "";

function setup_source(){
    elements["select_id"] = createSelect("Select Paint ID");
    for(var i in data){
	elements["select_id"].option(data[i][0])
    }
    elements["select_id"].changed( draw );
};

function plot( data ){
    console.log( "inside plot", data );
    console.log("ele: ", elements["select_id"] );
    console.log("val: ", elements["select_id"].value() );
    for(var i in data){
	if( data[i][0] == elements["select_id"].value() ){
	    var refs = Papa.parse(data[i][14]).data[0];
	    break;
	}
    }

    console.log("refs all: ", refs );
    var trace = {
	x: [],
	y: [],
	type: 'scatter'
    };
    
    for (i = 0, lambda = 380; lambda < 780.1; i++, lambda += 400/(refs.length-1)) {
	trace.y[i] = refs[i];
	trace.x[i] = lambda;
    }
        
    layout = { width: 800, //paper_bgcolor: "#222222",
	       height: 200,
	       margin: {l: 50,
			r: 20,
			t: 20,
			b: 40},
	       xaxis: {range: [380, 780],
		       title: "Wavelength [nm]"},
	       yaxis: {range: [0, 1],
		       title: "Reflectance"}
	     };
    
    Plotly.newPlot('plot-holder', [trace], layout);
};     

function draw(){
    plot(data);
};

    
document.getElementById('fileInput').addEventListener('change', function selectedFileChanged() {
    console.log("Loaded file:", this.files); // will contain information about the file that was selected.
    Papa.parse(document.getElementById('fileInput').files[0], {
	complete: function(results){
	    console.log( "Finished:", results.data);
	    data = results.data;
	    setup_source();
	}
    });
});
