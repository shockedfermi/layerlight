class Spectrum{

    constructor( ){ this.elements = {"incoming": [], "outgoing": [] }; };

    add_incoming_element( element ){ this.elements["incoming"].push( element ); };

    add_outgoing_element( element ){ this.elements["outgoing"].push( element ); };

    print(){
	for( key in this.elements ){
	    console.log( "This spectrum contains " + this.elements[key].length + " elements:" );
	    this.elements[key].forEach( function (item){
		item.print();
	    });
	}
    }

    get_total_amplitude( wavelength, p = new Vec( 0, 0, 0 ) ){
	let AIn = 0;
	this.elements["incoming"].forEach( function( item ){
	    AIn = item.pass( AIn, wavelength, p );
	});

	let AOut = 0;
	let RIn = 0;
	for( let index = this.elements["outgoing"].length - 1; index >= 0; index-- ){
	    let AOut_0 = AOut;
	    AOut = this.elements["outgoing"][index].pass( AOut_0, wavelength, p );
	    if( AOut_0 > AOut ) RIn += AOut_0 - AOut;
	};
	return RIn + AIn;
    };
    
    /*
    get_total_intensity( p = new Vec( 0, 0, 0 ) ){
	let II = 0;
	this.elements["incoming"].forEach( function( item ){
	    if( item.type.includes( "Source" ) ){
		II += item.get_intensity( p );
	    };
	});

	let IO = 0;
	this.elements["outgoing"].forEach( function( item ){
	    if( item.type.includes( "Source" ) ){
		IO += item.get_intensity( p );
	    };
	});

	return IO + II;
    };	
    */
};

class Source{

    constructor( intensity, shape = null, incoming = true ){
	this.intensity = intensity;
	this.shape = shape;
	this.max = 0;
    };

    norm(){ return this.intensity/this.max; }

    get_intensity( p ){
	if( this.shape == null || this.shape.pointInside( p ) ) {
	    return this.intensity;
	}else{
	    return 0;
	};
    };
    
    pass( amplitude, wavelength, p ){
	if( this.shape == null || this.shape.pointInside( p ) ) {
	    return amplitude  + this.get_amplitude( wavelength ) * this.norm();
	} else {
	    return amplitude;
	};
    };

    print(){ console.log( "Source object: " + this.type ); };
    
};

class numSource extends Source{

    constructor( amplitudes, intensity = 255, shape = null ){
	//Amplitudes should be an array of shape 81, containing the amplitude of each wavelength of width 5nm
	super( intensity, shape );
	this.type = "Numerical Source";
	this.amplitudes = amplitudes;
	for( key in this.amplitudes ){
	    if(this.amplitudes[key] > this.max){
		this.max = this.amplitudes[key];
	    }  
	}
    }

    get_amplitude( wavelength ){ return this.amplitudes[ (int)( (wavelength - 380 )/5 ) ]; };
}


class flatSource extends Source{

    constructor( lambda, spread = 1000, intensity = 255, shape = null ){
	super( intensity, shape );
	this.type = "Flat Source";
	this.lambda = lambda;
	this.spread = spread;
	this.max = 1;
    };

    get_amplitude( wavelength ){
	if( wavelength < this.lambda - this.spread/2 ){
	    return 0;
	} else if ( wavelength < this.lambda + this.spread/2 ) {
	    return 1;
	} else if ( wavelength >= this.lambda + this.spread/2 ) {
	    return 0;
	} else {
	    throw ValueError;
	};
    };
};

class blackbodySource extends Source{

    constructor( temperature, intensity = 1, shape = null ){
	super( intensity, shape );
	this.type = "Blackbody Source";
	this.temp = temperature;
	//Take the visible max
	var max_lambda = 2897771.955 / this.temp;
	this.max = this.get_amplitude( max_lambda );
	if( max_lambda < 380 ){ this.max = this.get_amplitude( 380 ); };
	if( max_lambda > 780 ){ this.max = this.get_amplitude( 780 ); };	   
    };

    get_amplitude( wavelength ){
	
	var wlm = wavelength * 1e-9;   /* Wavelength in meters */

	return (3.74183e-16 * Math.pow(wlm, -5.0)) /
            (Math.exp(1.4388e-2 / (wlm * this.temp)) - 1.0);
    };    
};

class gausSource extends Source{
    
    constructor( lambda, spread, intensity = 255, shape = null ){
	super( intensity, shape );
	this.type = "Gaussian Source";
	this.lambda = lambda;
	this.spread = spread;
	this.max = 1;
    };

    get_amplitude( wavelength ){ return Math.exp(-Math.pow( (wavelength - this.lambda)/this.spread, 2)/2); };
};

class Filter{
    
    constructor( shape = null ){ this.shape = shape; };

    T( wavelength ){ return 1; };

    pass( amplitude, wavelength, p ){
	if( amplitude == 0 ){
	    return 0;
	}else if( this.shape == null || this.shape.pointInside( p ) ) {
	    return amplitude * this.T( wavelength );
	}else{
	    return amplitude;
	}
    };

    print(){ console.log( "Filter object: " + this.type ); };
};

class numFilter extends Filter{

    constructor( amplitudes, shape = null ){
	//Amplitudes should be an array of shape 81, containing the amplitude of each wavelength of width 5nm
	super( shape );
	this.type = "Numerical Filter";
	this.amplitudes = amplitudes;
    }

    T( wavelength ){ return 1 - this.amplitudes[ (int)( (wavelength - 380 )/5 ) ] / 255; };
}

class gausFilter extends Filter{
    
    constructor( lambda, spread, shape = null ){
	super( shape );
	this.type = "Gaussian Filter";
	this.lambda = lambda;
	this.spread = spread;
    };

    T( wavelength ){ return 1 - Math.exp(-Math.pow( (wavelength - this.lambda)/this.spread, 2)/2); };
};

class inverseGausFilter extends Filter{
    
    constructor( lambda, spread, shape = null ){
	super( shape );
	this.type = "Inverse Gaussian Filter";
	this.lambda = lambda;
	this.spread = spread;
    };

    T( wavelength ){ return Math.exp(-Math.pow( (wavelength - this.lambda)/this.spread, 2)/2); };
  
};

class flatFilter extends Filter{
    
    constructor( lambda, spread, shape = null ){
	super( shape );
	this.type = "Flat Filter";
	this.lambda = lambda;
	this.spread = spread;
    };

    
    T( wavelength ){
	if( wavelength < this.lambda - this.spread/2 ){
	    return 1;
	} else if ( wavelength < this.lambda + this.spread/2 ) {
	    return 0;
	} else if ( wavelength >= this.lambda + this.spread/2 ) {
	    return 1;
	} else {
	    throw ValueError;
	};
    };
    
};

class inverseFlatFilter extends Filter{
    
    constructor( lambda, spread, shape = null ){
	super( shape );
	this.type = "Flat Filter";
	this.lambda = lambda;
	this.spread = spread;
    };

    
    T( wavelength ){
	if( wavelength < this.lambda - this.spread/2 ){
	    return 0;
	} else if ( wavelength < this.lambda + this.spread/2 ) {
	    return 1;
	} else if ( wavelength >= this.lambda + this.spread/2 ) {
	    return 0;
	} else {
	    throw ValueError;
	};
    };
    
};

class Reflector{
    
    constructor( shape = null ){
	this.shape = shape;
    };

    R( wavelength ){
	return 0;
    };

    pass( amplitude, wavelength, p ){
	if( amplitude == 0 ){
	    return 0;
	}else if( this.shape == null || this.shape.pointInside( p ) ) {
	    return amplitude * this.T( wavelength );
	}else{
	    return amplitude;
	}
    };

    print(){ console.log( "Reflector object: " + this.type ); };
    
};
