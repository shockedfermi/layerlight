let cie_colour_match = [ [0.0014,0.0000,0.0065], [0.0022,0.0001,0.0105], [0.0042,0.0001,0.0201],
			 [0.0076,0.0002,0.0362], [0.0143,0.0004,0.0679], [0.0232,0.0006,0.1102],
			 [0.0435,0.0012,0.2074], [0.0776,0.0022,0.3713], [0.1344,0.0040,0.6456],
			 [0.2148,0.0073,1.0391], [0.2839,0.0116,1.3856], [0.3285,0.0168,1.6230],
			 [0.3483,0.0230,1.7471], [0.3481,0.0298,1.7826], [0.3362,0.0380,1.7721],
			 [0.3187,0.0480,1.7441], [0.2908,0.0600,1.6692], [0.2511,0.0739,1.5281],
			 [0.1954,0.0910,1.2876], [0.1421,0.1126,1.0419], [0.0956,0.1390,0.8130],
			 [0.0580,0.1693,0.6162], [0.0320,0.2080,0.4652], [0.0147,0.2586,0.3533],
			 [0.0049,0.3230,0.2720], [0.0024,0.4073,0.2123], [0.0093,0.5030,0.1582],
			 [0.0291,0.6082,0.1117], [0.0633,0.7100,0.0782], [0.1096,0.7932,0.0573],
			 [0.1655,0.8620,0.0422], [0.2257,0.9149,0.0298], [0.2904,0.9540,0.0203],
			 [0.3597,0.9803,0.0134], [0.4334,0.9950,0.0087], [0.5121,1.0000,0.0057],
			 [0.5945,0.9950,0.0039], [0.6784,0.9786,0.0027], [0.7621,0.9520,0.0021],
			 [0.8425,0.9154,0.0018], [0.9163,0.8700,0.0017], [0.9786,0.8163,0.0014],
			 [1.0263,0.7570,0.0011], [1.0567,0.6949,0.0010], [1.0622,0.6310,0.0008],
			 [1.0456,0.5668,0.0006], [1.0026,0.5030,0.0003], [0.9384,0.4412,0.0002],
			 [0.8544,0.3810,0.0002], [0.7514,0.3210,0.0001], [0.6424,0.2650,0.0000],
			 [0.5419,0.2170,0.0000], [0.4479,0.1750,0.0000], [0.3608,0.1382,0.0000],
			 [0.2835,0.1070,0.0000], [0.2187,0.0816,0.0000], [0.1649,0.0610,0.0000],
			 [0.1212,0.0446,0.0000], [0.0874,0.0320,0.0000], [0.0636,0.0232,0.0000],
			 [0.0468,0.0170,0.0000], [0.0329,0.0119,0.0000], [0.0227,0.0082,0.0000],
			 [0.0158,0.0057,0.0000], [0.0114,0.0041,0.0000], [0.0081,0.0029,0.0000],
			 [0.0058,0.0021,0.0000], [0.0041,0.0015,0.0000], [0.0029,0.0010,0.0000],
			 [0.0020,0.0007,0.0000], [0.0014,0.0005,0.0000], [0.0010,0.0004,0.0000],
			 [0.0007,0.0002,0.0000], [0.0005,0.0002,0.0000], [0.0003,0.0001,0.0000],
			 [0.0002,0.0001,0.0000], [0.0002,0.0001,0.0000], [0.0001,0.0000,0.0000],
			 [0.0001,0.0000,0.0000], [0.0001,0.0000,0.0000], [0.0000,0.0000,0.0000] ];

let IlluminantC   = [ 0.3101, 0.3162 ];
let IlluminantD65 = [ 0.3127, 0.3291 ];
let IlluminantE   = [ 0.33333333, 0.33333333 ];

class colourSystem{
    constructor( name, xRed, yRed, xGreen, yGreen, xBlue, yBlue, xWhite, yWhite ){
        this.name = name;
        this.xRed = xRed;
        this.yRed = yRed;
        this.xGreen = xGreen;
        this.yGreen = yGreen;
        this.xBlue = xBlue;
        this.yBlue = yBlue;
        this.xWhite = xWhite;
        this.yWhite = yWhite;
    };
};
        

let colourSystemDic = { "NTSCsystem":   [ "NTSC",            0.67,   0.33,   0.21,   0.71,   0.14,   0.08,   IlluminantC[0],   IlluminantC[1]   ],
			"EBUsystem":    [ "EBU (PAL/SECAM)", 0.64,   0.33,   0.29,   0.60,   0.15,   0.06,   IlluminantD65[0], IlluminantD65[1] ],
			"SMPTEsystem":  [ "SMPTE",           0.630,  0.340,  0.310,  0.595,  0.155,  0.070,  IlluminantD65[0], IlluminantD65[1] ],
			"HDTVsystem":   [ "HDTV",            0.670,  0.330,  0.210,  0.710,  0.150,  0.060,  IlluminantD65[0], IlluminantD65[1] ],
			"CIEsystem":    [ "CIE",             0.7355, 0.2645, 0.2658, 0.7243, 0.1669, 0.0085, IlluminantE[0],   IlluminantE[1]   ],
			"Rec709system": [ "CIE REC 709",     0.64,   0.33,   0.30,   0.60,   0.15,   0.06,   IlluminantD65[0], IlluminantD65[1] ] };

function spectrum_to_xyz( spectrum, p = new Vec( 0, 0, 0 ), step = 5 ){
    
    if( step < 5 ){ step = 5; };

    var X, Y, Z;
    X = Y = Z = 0;
    var max_amp = 0;
    for (i = 0, lambda = 380; lambda < 780.1; i++, lambda += 5) {
	amplitude = spectrum.get_total_amplitude( lambda, p );
	if( amplitude > max_amp ){
	    max_amp = amplitude;
	};
	X += amplitude * cie_colour_match[i][0];
	Y += amplitude * cie_colour_match[i][1];
	Z += amplitude * cie_colour_match[i][2];
    }
    
    var XYZ = (X + Y + Z);
    if( XYZ == 0 ){
	XYZ = 1;
    };
    var x = X / XYZ;
    var y = Y / XYZ;
    var z = Z / XYZ;
    
    return [x, y, z, max_amp];
};


function xyz_to_rgb( xc, yc, zc, cs){

    var xr, yr, zr, xg, yg, zg, xb, yb, zb;
    var xw, yw, zw;
    var rx, ry, rz, gx, gy, gz, bx, by, bz;
    var rw, gw, bw;
    
    xr = cs.xRed;    yr = cs.yRed;    zr = 1 - (xr + yr);
    xg = cs.xGreen;  yg = cs.yGreen;  zg = 1 - (xg + yg);
    xb = cs.xBlue;   yb = cs.yBlue;   zb = 1 - (xb + yb);

    xw = cs.xWhite;  yw = cs.yWhite;  zw = 1 - (xw + yw);

    /* xyz -> rgb matrix, before scaling to white. */

    rx = (yg * zb) - (yb * zg);  ry = (xb * zg) - (xg * zb);  rz = (xg * yb) - (xb * yg);
    gx = (yb * zr) - (yr * zb);  gy = (xr * zb) - (xb * zr);  gz = (xb * yr) - (xr * yb);
    bx = (yr * zg) - (yg * zr);  by = (xg * zr) - (xr * zg);  bz = (xr * yg) - (xg * yr);

    /* White scaling factors.
    #   Dividing by yw scales the white luminance to unity, as conventional. */

    rw = ((rx * xw) + (ry * yw) + (rz * zw)) / yw;
    gw = ((gx * xw) + (gy * yw) + (gz * zw)) / yw;
    bw = ((bx * xw) + (by * yw) + (bz * zw)) / yw;

    /* xyz -> rgb matrix, correctly scaled to white. */

    rx = rx / rw;  ry = ry / rw;  rz = rz / rw;
    gx = gx / gw;  gy = gy / gw;  gz = gz / gw;
    bx = bx / bw;  by = by / bw;  bz = bz / bw;

    /* rgb of the desired point */

    var r = (rx * xc) + (ry * yc) + (rz * zc);
    var g = (gx * xc) + (gy * yc) + (gz * zc);
    var b = (bx * xc) + (by * yc) + (bz * zc);

    return [r, g, b];
};

function constrain_rgb(r, g, b)
{
    var w;

    /* Amount of white needed is w = - min(0, *r, *g, *b) */

    if( 0 < r ) { w = 0; } else { w = r };
    if( w < g ) { w = w; } else { w = g };
    if( w < b ) { w = w; } else { w = b };
    w = -w;

    /* Add just enough white to make r, g, b all positive. */

    if (w > 0) {
        r += w;  g += w; b += w;
    }
    return [r, g, b];
}

function is_outside( r, g, b ){
    var w;
    
    /* Amount of white needed is w = - min(0, *r, *g, *b) */

    if( 0 < r ) { w = 0; } else { w = r };
    if( w < g ) { w = w; } else { w = g };
    if( w < b ) { w = w; } else { w = b };
    w = -w;

    return (w > 0);
};

function scale_rgb(r, g, b, scale = 255)
{
    var greatest = Math.max( r, g, b );
    if (greatest > 0) {
        r *= scale/greatest;
        g *= scale/greatest;
        b *= scale/greatest;
    }
    return [r, g, b];
}   

function ltr( spectrum, p = new Vec( 0, 0, 0 ) ){

    var cs = new colourSystem( ...colourSystemDic["NTSCsystem"] );
    //console.log( spectrum );
    var [x, y, z, max_amp] = spectrum_to_xyz( spectrum, p );
    //console.log( "x: ", x, " y: ", y, " z: ", z );
    var [r, g, b] = xyz_to_rgb( x, y, z, cs );
    //console.log( "R: ", R, " G: ", G, " B: ", B );
    //if( is_outside( R, G, B ) ){ console.log( "Fake color" ); };
    [R, G, B] = scale_rgb( ...constrain_rgb( r, g, b ), max_amp);
    
    //console.log( "r: ", r, " g: ", g, " b: ", b);
    return [R, G, B];
};
