//RGB light components
var R, G, B;
R = G = B = 255;

//To be assigned variables
var filter_count = 0;
var source_count = 0;
var element_count = 0;
var elements = {};
var mousePanels = {};

var RESOLUTION;
var PIXEL_SIZE;
var background_color;
var text_color;

var screen_size;
var canvas_size;
var screen_pos;
var screen_text_pos;

var shapes = {};
var cursor_pos = new Vec( 0, 0 );

//Option parameters
var h_space = 230;
var v_space = 40;
var slider_width = 200;

var addIncomingSourceButtonPosition  = [ 10, 10 ];
var addFilterButtonPosition  = [ 10 + h_space - 40 , 10 ];
var addOutgoingSourceButtonPosition  = [ 2*h_space, 10 ];
var addReflectorButtonPosition  = [ 3*h_space - 40, 10 ];
var resolutionSelectPosition = [ 7*h_space/2  , 10 ];
var resetButtonPosition      = [ 4*h_space, 10 ];
var exampleSelectPosition   = [ 9*h_space/2, 10 ];

var addIncomingSourceButtonLabel = "Add incoming source";
var addFilterButtonLabel         = "Add filter";
var addOutgoingSourceButtonLabel = "Add outgoing source";
var addReflectorButtonLabel      = "Add reflector";
var resetButtonLabel             = "Reset";

var source_select_ypos    =   v_space;
var sub_select_ypos       = 2*v_space;
var intensity_slider_ypos = 3*v_space;
var lambda_slider_ypos    = 4*v_space;
var spread_slider_ypos    = 5*v_space;
var T_slider_ypos         = 4*v_space;

var filter_select_ypos        =   v_space;
var filter_lambda_slider_ypos = 3*v_space;
var filter_spread_slider_ypos = 4*v_space;

var gausSourceStr   = "Gaussian";
var monoSourceStr   = "Monochromatic";
var flatSourceStr   = "Flat Spectrum";
var bbSourceStr     = "Blackbody Spectrum";
var drawSourceStr   = "Draw a spectrum";

var flatStr         = "Flat";
var invFlatStr      = "Inv. Flat";
var gausStr         = "Gaussian";
var invGausStr      = "Inv. Gaussian";
var drawFilterStr   = "Draw a spectrum";
var importFilterStr = "Import a spectrum";

function get_element_xpos( element_count ){
    return element_count * h_space;
};

//Start panel (Always visible)
function setup() {
    
    //Drawing properties
    canvas_size = new Vec( windowWidth, 700 );
    screen_size = new Vec( canvas_size.y/2, canvas_size.y/2 );

    RESOLUTION = 1; //sqrt(pixels)
    PIXEL_SIZE = Math.ceil( screen_size.x / RESOLUTION );
    background_color = [22, 22, 22];
    text_color       = [230, 230, 230];
    
    screen_pos      = new Vec( canvas_size.x/2 - screen_size.x/2, canvas_size.y/2 - 50 );
    screen_text_pos = new Vec( screen_pos.x + screen_size.x/3, screen_pos.y + screen_size.y*1.1 );
    cursor_pos      = new Vec( screen_pos.x + screen_size.x/2, screen_pos.y + screen_size.y/2 );

    mousePanels["screen"] = {"dimensions": [ screen_pos, screen_size ] };
    
    shapes["Triangle A"] = new Triangle( new Vec( screen_pos.x*1.1,               screen_pos.y*1.1,                ),
					 new Vec( screen_pos.x*1.1,               screen_pos.y + screen_size.y*9/10 ),
					 new Vec( screen_pos.x + screen_size.x*2/3, screen_pos.y + screen_size.y/2 ) );
    
    shapes["Triangle B"] = new Triangle( new Vec( screen_pos.x + screen_size.x*9/10, screen_pos.y*1.1,                ),
					 new Vec( screen_pos.x + screen_size.x*1/3,  screen_pos.y + screen_size.y/2  ),
					 new Vec( screen_pos.x + screen_size.x*9/10, screen_pos.y + screen_size.y*9/10 ) );
    
    
    var starPs = { 0: new Vec( screen_pos.x + 4*screen_size.x/5 , screen_pos.y + 9*screen_size.y/10) };
    for( ipos = 1; ipos != 5; ipos ++ ){
	var rot_x = Math.cos( Math.PI + ipos * 72 * Math.PI / 180 );
	var rot_y = Math.sin( ipos * 72 * Math.PI / 180 );
	starPs[ipos] = starPs[ipos - 1].add( new Vec( rot_x, rot_y ) );
    }
    shapes["Star"] = new Star( starPs[0],
			       starPs[1],
			       starPs[2],
			       starPs[3],
			       starPs[4] );
    shapes["Star"].scale( -280 );
    
    shapes["Moon"] = new Moon( new Vec( screen_pos.x + screen_size.x/3, screen_pos.y + 20 ),
			       new Vec( screen_pos.x + screen_size.x/4, screen_pos.y + screen_size.y - 20) );
    

    var canvas = createCanvas( canvas_size.x, canvas_size.y );
    canvas.parent('sketch-holder');
    background( ...background_color );

    addIncomingSourceButton = createButton( "Add incoming source" );
    addIncomingSourceButton.id( "incoming" );
    addIncomingSourceButton.position( ...addIncomingSourceButtonPosition );
    addIncomingSourceButton.mouseClicked( addSource );

    addFilterButton = createButton( "Add filter" );
    addFilterButton.id( "filter" );
    addFilterButton.position( ...addFilterButtonPosition );
    addFilterButton.mouseClicked( addFilter );

    addOutgoingSourceButton = createButton( "Add outgoing source" );
    addOutgoingSourceButton.id( "outgoing" );
    addOutgoingSourceButton.position( ...addOutgoingSourceButtonPosition );
    addOutgoingSourceButton.mouseClicked( addSource );

    addReflectorButton = createButton( "Add reflector" );
    addReflectorButton.id( "reflector" );
    addReflectorButton.position( ...addReflectorButtonPosition );
    addReflectorButton.mouseClicked( addFilter );

    resetButton = createButton( "Reset" );
    resetButton.position( ...resetButtonPosition );
    resetButton.mouseClicked( resetEvent );

    resolutionSelect = createSelect();
    resolutionSelect.option("1x1");
    resolutionSelect.option("10x10");
    resolutionSelect.option("50x50");
    resolutionSelect.position( ...resolutionSelectPosition );
    resolutionSelect.changed( resolutionEvent );

    exampleSelect = createSelect();
    exampleSelect.option("Examples");
    exampleSelect.disable("Examples");
    exampleSelect.selected("Examples");
    exampleSelect.option("Backlighting");
    exampleSelect.option("Color Shadow");
    exampleSelect.option("Wavelength Sweep");
    exampleSelect.position( exampleSelectPosition );
    exampleSelect.changed( exampleSelectEvent );

    
    updatePanel();
    draw_hist( spectrum );
    noLoop();
}

function updatePanel(){
    
    background( ...background_color );
    
    fill( ...text_color );
    for( var key in elements ){
	for( var key2 in elements[key] ){
	    if( key2.includes("slider") ){
		let slider = elements[key][key2];
		text( slider.value(), slider.position()['x'] + h_space/2, slider.position()['y'] );
	    } else if( elements[key]["type"] && elements[key]["type"].includes( "draw" ) ){
		strokeWeight( 0 );
		fill( 255, 255, 255 );
		text( "λ",
		      mousePanels[key]["dimensions"][0].x + mousePanels[key]["dimensions"][1].x/2,
		      mousePanels[key]["dimensions"][0].y + mousePanels[key]["dimensions"][1].y + 15 );
		for( const [index, item] of elements[key]["amplitudes"].entries() ){
		    xsize = mousePanels[key]["dimensions"][1].x / 81;
		    xpos  = mousePanels[key]["dimensions"][0].x + index * xsize;
		    ysize = item * mousePanels[key]["dimensions"][1].y / 255;
		    ypos  = mousePanels[key]["dimensions"][0].y + mousePanels[key]["dimensions"][1].y - ysize;
		    
		    rect( xpos, ypos, xsize, ysize );
		}
	    }
	}
    }
    draw();
};

//Final draw function
function draw() {

    for( var vi = 0; vi != RESOLUTION; vi++ ){
	for( var hi = 0; hi != RESOLUTION; hi++ ){

	    pixel_pos    = new Vec( screen_pos.x + vi*PIXEL_SIZE, screen_pos.y + hi*PIXEL_SIZE);
	    pixel_center = new Vec( pixel_pos.x  + PIXEL_SIZE/2.0, pixel_pos.y + PIXEL_SIZE/2.0);
	    
	    //Create spectrum
	    spectrum = new Spectrum();
	    for( var key in elements ){
		if( elements[key]["type"] ){
		    if( key.includes( "outgoing" ) || key.includes( "reflector" ) ){
			spectrum.add_outgoing_element( make_element( key ) );
		    } else {
			spectrum.add_incoming_element( make_element( key ) );
		    }
		}
	    }

	    var [R, G, B ] = ltr( spectrum, pixel_center );
	    //console.log( "R: " + R +" G: " + G + " B: " + B );
	    R = Math.floor( R );
	    G = Math.floor( G );
	    B = Math.floor( B );
	    
	    //Result
	    fill( R, G, B );
	    strokeWeight( 0 );
	    rect( pixel_pos.x, pixel_pos.y, PIXEL_SIZE, PIXEL_SIZE );

	    if( insideRect( cursor_pos, pixel_pos, new Vec( PIXEL_SIZE, PIXEL_SIZE ) ) ){
		fill( ...text_color );
		text( "R: " + R + ", G: " + G + ", B: " + B, screen_text_pos.x, screen_text_pos.y );
		
		fill( 0, 0, 0 );
		strokeWeight( 2 );
		var cursor = {x: line( cursor_pos.x - 10, cursor_pos.y, cursor_pos.x + 10, cursor_pos.y ),
			      y: line( cursor_pos.x, cursor_pos.y - 10, cursor_pos.x, cursor_pos.y + 10 ) };

		draw_hist( spectrum, pixel_center );
	    }

	}
    }
}

function insideRect( pos, rectPos, rectSize ){
    return pos.x > rectPos.x && 
	pos.x < rectPos.x + rectSize.x && 
	pos.y > rectPos.y &&
	pos.y < rectPos.y + rectSize.y;
};
    
//https://github.com/processing/p5.js/wiki/Positioning-your-canvas
function windowResized() {
    setup();
}
    
function mouseDragged(){
    for( key in mousePanels ){
	if( insideRect( new Vec( mouseX, mouseY ), ...mousePanels[key]["dimensions"] ) ){
	    if( key == "screen" ){
		cursor_pos = new Vec( mouseX, mouseY );
	    } else if( !insideRect( new Vec( mouseX, mouseY ), ...mousePanels[key]["ignore"] ) ) {
		index = Math.floor( 81 * ( mouseX - mousePanels[key]["dimensions"][0].x ) / mousePanels[key]["dimensions"][1].x );
		elements[key]["amplitudes"][index] = 255*( mousePanels[key]["dimensions"][0].y + mousePanels[key]["dimensions"][1].y - mouseY) / mousePanels[key]["dimensions"][1].y;
		mousePanels[key]["ignore"] = [ new Vec( mousePanels[key]["dimensions"][0].x + index * xsize, mousePanels[key]["dimensions"][0].y ), new Vec( mousePanels[key]["dimensions"][1].x / 81, mousePanels[key]["dimensions"][1].y ) ];
	    }
	    updatePanel();
	}
    }
};

function resolutionEvent(){
    RESOLUTION = parseInt( this.value().split("x")[0] );
    PIXEL_SIZE = screen_size.x / RESOLUTION;
    draw();
};

function addSource(  ){

    if( element_count >= 8){
	console.log( "I don't want to create anymore elements!" );
	throw( ValueError );
    }

    ID = this.id() + "_source_" + ++source_count;
    elements[ ID ] = {};

    elements[ ID ]["select"] = createSelect();
    elements[ ID ]["select"].id( ID );
    elements[ ID ]["select"].style('width', slider_width + 'px');
    elements[ ID ]["select"].option("Select an " + this.id() + " source.");
    elements[ ID ]["select"].disable("Select an " + this.id() + " source.");
    elements[ ID ]["select"].selected("Select an " + this.id() + " source.");
    elements[ ID ]["select"].option(gausSourceStr);
    elements[ ID ]["select"].option(monoSourceStr);
    elements[ ID ]["select"].option(flatSourceStr);
    elements[ ID ]["select"].option(bbSourceStr);
    elements[ ID ]["select"].option(drawSourceStr);
    elements[ ID ]["select"].position( get_element_xpos(element_count++), source_select_ypos );
    elements[ ID ]["select"].changed( sourceSelectEvent );
};

function addFilter(){

    if( element_count >= 8){
	console.log( "I don't want to create anymore elements!" );
	throw( ValueError );
    }
    ID = this.id() + "_" + ++filter_count;
    elements[ ID ] = {};

    elements[ ID ]["select"] = createSelect();
    elements[ ID ]["select"].id( ID );
    elements[ ID ]["select"].style('width', slider_width + 'px');
    elements[ ID ]["select"].option("Choose " + this.id() );
    elements[ ID ]["select"].disable("Choose " + this.id() );
    elements[ ID ]["select"].selected("Choose " + this.id() );
    elements[ ID ]["select"].option(flatStr);
    elements[ ID ]["select"].option(invFlatStr);
    elements[ ID ]["select"].option(gausStr);
    elements[ ID ]["select"].option(invGausStr);
    elements[ ID ]["select"].option(drawFilterStr);
    elements[ ID ]["select"].option(importFilterStr);
    elements[ ID ]["select"].position( get_element_xpos(element_count++), source_select_ypos );
    elements[ ID ]["select"].changed( filterSelectEvent );
};

//Selection of light subpanel
function sourceSelectEvent( ){

    for( key in elements[ this.id() ] ){
	if ( key != "select" ){
	    try{ elements[ this.id() ][key].remove(); } catch( ReferenceError ){};
	};
    };

    elements[ this.id() ]["shape_select"] = createSelect( );
    elements[ this.id() ]["shape_select"].position( this.position()['x'], sub_select_ypos );
    elements[ this.id() ]["shape_select"].style('width', slider_width + 'px');
    elements[ this.id() ]["shape_select"].option("No shape");
    elements[ this.id() ]["shape_select"].option("Triangle A");
    elements[ this.id() ]["shape_select"].option("Triangle B");
    elements[ this.id() ]["shape_select"].option("Star");
    elements[ this.id() ]["shape_select"].option("Moon");
    elements[ this.id() ]["shape_select"].changed( updatePanel );
    
    elements[ this.id() ]["intensity_slider"] = createSlider( 0, 255, 255 );
    elements[ this.id() ]["intensity_slider"].position( this.position()['x'], intensity_slider_ypos);
    elements[ this.id() ]["intensity_slider"].style('width', slider_width + 'px');
    elements[ this.id() ]["intensity_slider"].changed( updatePanel );
    elements[ this.id() ]["unit"] = "";
    
    if        (this.value() == monoSourceStr){
	monochromaticSetup( this.id() );
    } else if (this.value() == flatSourceStr){
	elements[ this.id() ]["type"] = "flat_source";
	elements[ this.id() ]["unit"] = " nm";
	updatePanel( this.id() );
    } else if (this.value() == bbSourceStr){
	blackbodySetup( this.id() );
    } else if (this.value() == gausSourceStr){
	gausSourceSetup( this.id() );
    } else if (this.value() == drawSourceStr){
	drawSourceSetup( this.id() );
    };
    updatePanel();
};

//Selection of filter subpanel
function filterSelectEvent(){

    for( key in elements[ this.id() ] ){
	if ( key != "select" ){
	    try{ elements[ this.id() ][key].remove(); } catch( ReferenceError ){};
	};
    };
    
    elements[ this.id() ]["shape_select"] = createSelect( );
    elements[ this.id() ]["shape_select"].position( this.position()['x'], sub_select_ypos );
    elements[ this.id() ]["shape_select"].style('width', slider_width + 'px');
    elements[ this.id() ]["shape_select"].option("No shape");
    elements[ this.id() ]["shape_select"].option("Triangle A");
    elements[ this.id() ]["shape_select"].option("Triangle B");
    elements[ this.id() ]["shape_select"].option("Star");
    elements[ this.id() ]["shape_select"].option("Moon");
    elements[ this.id() ]["shape_select"].changed( updatePanel );
    
    if (this.value() == flatStr){
	flatFilterSetup( this.id(), inv = false );
    } else if (this.value() == invFlatStr){
	flatFilterSetup( this.id(), inv = true );
    } else if (this.value() == gausStr){
	gausFilterSetup( this.id(), inv = false );
    } else if (this.value() == invGausStr){
	gausFilterSetup( this.id(), inv = true );
    } else if (this.value() == drawFilterStr){
	drawFilterSetup( this.id() );
    } else if (this.value() == importFilterStr){
	importFilterSetup( this.id() );
    }
    updatePanel();
};



function resetEvent(){
    
    background( ...background_color );

    for( var key in elements ){
	for( var key2 in elements[key] ){
	    try{
		elements[key][key2].remove();
	    } catch(TypeError){};
	}
	delete elements[key];
    }
    filter_count = 0;
    source_count = 0;
    element_count = 0;
    draw();
};

function exampleSelectEvent(){

    resetEvent();
    
    for( var key in elements ){
	for( var key2 in elements[key] ){
	    try{
		elements[key][key2].remove();
	    } catch(TypeError){};
	}
	delete elements[key];
    }
    filter_count = 0;
    source_count = 0;
    element_count = 0;
    draw();
};
    
function draw_hist( spectrum, p ) {
    var trace = {
	x: [],
	y: [],
	type: 'scatter'
    };
    
    for (i = 0, lambda = 380; lambda < 780.1; i++, lambda += 5) {
	trace.y[i] = spectrum.get_total_amplitude( lambda, p );
	trace.x[i] = lambda;
    }
        
    layout = { width: canvas_size.x, //paper_bgcolor: "#222222",
	       height: 200,
	       margin: {l: 50,
			r: 20,
			t: 20,
			b: 40},
	       xaxis: {range: [380, 780],
		       title: "Wavelength [nm]"},
	       yaxis: {range: [0, 255],
		       title: "Intensity"}
	     };
    
    Plotly.newPlot('plot-holder', [trace], layout);
}


function makeLambdaSlider( element, min, max, def, name = "lambda_slider" ){
    element[name] = createSlider( min, max, def );
    element[name].position( element["select"].position()['x'], lambda_slider_ypos );
    element[name].style('width', slider_width + 'px' );
    element[name].changed( updatePanel );
}

function makeSpreadSlider( element, min, max, def, name = "spread_slider" ){
    element[name] = createSlider( min, max, def );
    element[name].position( element["select"].position()['x'], spread_slider_ypos );
    element[name].style('width', slider_width + 'px' );
    element[name].changed( updatePanel );
}

//Draw light panel
function drawSourceSetup( ID ){
    elements[ ID ]["type"] = "draw_source";
    mousePanels[ ID ] = { "dimensions": [ new Vec( elements[ ID ]["select"].position()['x'] + 5, lambda_slider_ypos ), new Vec( h_space - 5, 100 ) ],
			  "ignore": [ new Vec(), new Vec() ] };
    elements[ ID ]["amplitudes"] = Array(81).fill(255);
};

//Monochromatic light panel
function monochromaticSetup( ID ){
    elements[ ID ]["type"] = "monochromatic_source";
    makeLambdaSlider( elements[ID], 380, 780, 500 );
};

//Blackbody light panel
function blackbodySetup( ID ){
	
    elements[ ID ]["type"] = "blackbody_source";
    elements[ ID ]["T_slider"] = createSlider( 100, 20000, 5700 );
    elements[ ID ]["T_slider"].position( elements[ ID ]["select"].position()['x'], T_slider_ypos );
    elements[ ID ]["T_slider"].style('width', slider_width + 'px');
    elements[ ID ]["T_slider"].changed( updatePanel );
};

//Gaussian light panel
function gausSourceSetup( ID ){
    elements[ ID ]["type"] = "gaussian_source";
    makeLambdaSlider( elements[ID], 100, 1000, 500 );
    makeSpreadSlider( elements[ID], 1, 300, 30 );

};

//Draw Filter panel
function drawFilterSetup( ID ){
    elements[ ID ]["type"] = "draw_filter";
    mousePanels[ ID ] = { "dimensions": [ new Vec( elements[ ID ]["select"].position()['x'] + 5, lambda_slider_ypos ), new Vec( h_space - 5, 100 ) ],
			  "ignore": [ new Vec(), new Vec() ] };
    elements[ ID ]["amplitudes"] = Array(81).fill(255);
};

//Import Filter panel
function importFilterSetup( ID ){
    elements[ ID ]["type"] = "import_filter";

    document.getElementById('fileInput').addEventListener('change', function selectedFileChanged() {
    console.log("Loaded file:", this.files); // will contain information about the file that was selected.
    Papa.parse(document.getElementById('fileInput').files[0], {
	complete: function(results){
	    console.log( "Finished:", results.data);
	    data = results.data;
	    elements["select_id"] = createSelect("Select Paint ID");
	    for(var i in data) elements["select_id"].option(data[i][0]);
	    elements["select_id"].changed( draw );
	}
    });
    });
   
};

//Gaussian Filter panel
function gausFilterSetup( ID, inv = false ){
    elements[ ID ]["type"] = [ "gaussian_filter", "inverse gaussian_filter" ][ int(inv) ];
    makeLambdaSlider( elements[ID], 380, 780, 500 );
    makeSpreadSlider( elements[ID], 1, 300, 30 );
};

//Flat Filter panel
function flatFilterSetup( ID, inv = false ){
    elements[ ID ]["type"] = [ "flat_filter", "inverse flat_filter" ][ int(inv) ];
    makeLambdaSlider( elements[ID], 380, 780, 500 );
    makeSpreadSlider( elements[ID], 1, 300, 30 );
};

function make_element( key ){

    shape = shapes[ elements[key]["shape_select"].value() ];
    
    if( elements[key]["type"].includes("source") ){
	
	intensity = elements[key]["intensity_slider"].value();
	
	switch( elements[key]["type"].slice( 0, -7 ) ){
	case "monochromatic":
	    lambda = elements[key]["lambda_slider"].value();
	    return new flatSource( lambda, 5, intensity, shape );
	case "gaussian":
	    lambda = elements[key]["lambda_slider"].value();
	    spread = elements[key]["spread_slider"].value();
	    return new gausSource( lambda, spread, intensity, shape );
	case "flat":
	    return new flatSource( 530, 1000, intensity, shape );
	case "blackbody":
	    T = elements[key]["T_slider"].value();
	    return new blackbodySource( T, intensity, shape );
	case "draw":
	    return new numSource( elements[key]["amplitudes"], intensity, shape );
	};
    };
     
    if( elements[key]["type"].includes("filter") ){
	
	if( elements[key]["type"].slice( 0, -7 ) == "draw" ){
	    return new numFilter( elements[key]["amplitudes"], shape );
	}
	
	lambda = elements[key]["lambda_slider"].value();
	spread = elements[key]["spread_slider"].value();
	switch( elements[key]["type"].slice( 0, -7 ) ){
	case "gaussian":
	    return new gausFilter( lambda, spread, shape );
	case "inverse gaussian":
	    return new inverseGausFilter( lambda, spread, shape );
	case "flat":
	    return new flatFilter( lambda, spread, shape );
	case "inverse flat":
	    return new inverseFlatFilter( lambda, spread, shape );
	}
    }
    throw ValueError;
};
